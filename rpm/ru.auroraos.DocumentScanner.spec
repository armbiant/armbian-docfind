%define __provides_exclude_from ^%{_datadir}/%{name}/lib/.*$
%define __requires_exclude ^libopencv.*$

Name: ru.auroraos.DocumentScanner
Summary: Document Scanner
Version: 0.1.0
Release: 1
License: BSD-3-Clause
Source0: %{name}-%{version}.tar.bz2
BuildRequires: pkgconfig(auroraapp)
BuildRequires: pkgconfig(protobuf)
BuildRequires: pkgconfig(nemonotifications-qt5)
BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5Qml)
BuildRequires: pkgconfig(Qt5Quick)
BuildRequires: pkgconfig(Qt5Concurrent)
BuildRequires: pkgconfig(Qt5Gui)
BuildRequires: pkgconfig(Qt5OpenGL)
BuildRequires: pkgconfig(Qt5DBus)
BuildRequires: cmake >= 2.6.3
Requires: sailfishsilica-qt5 >= 0.10.9
Requires: nemo-qml-plugin-notifications-qt5

%description
The project provides an example of using video filters to finding documents.

%prep
%autosetup

%build
%qmake5 -r OPENCV_MFLAGS=%{?_smp_mflags}
make %{?_smp_mflags}

%install
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
