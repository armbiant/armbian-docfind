################################################################################
##
## Copyright (C) 2021-2022 Open Mobile Platform LLC.
## Contact: https://community.omprussia.ru/open-source
##
## This file is part of the Document Scanner project.
##
## Redistribution and use in source and binary forms,
## with or without modification, are permitted provided
## that the following conditions are met:
##
## * Redistributions of source code must retain the above copyright notice,
##   this list of conditions and the following disclaimer.
## * Redistributions in binary form must reproduce the above copyright notice,
##   this list of conditions and the following disclaimer
##   in the documentation and/or other materials provided with the distribution.
## * Neither the name of the copyright holder nor the names of its contributors
##   may be used to endorse or promote products derived from this software
##   without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
## THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
## FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
## IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
## FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
## OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
## PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
## LOSS OF USE, DATA, OR PROFITS;
## OR BUSINESS INTERRUPTION)
## HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
## WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
## EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
################################################################################

TEMPLATE = app

TARGET = ru.auroraos.DocumentScanner

QT += multimedia concurrent sql dbus

DEFINES += \
    DBUS_SERVICE=\\\"ru.auroraos.DocumentScanner\\\" \
    DBUS_PATH=\\\"/ru/auroraos/DocumentScanner\\\" \
    DBUS_INTERFACE=\\\"ru.auroraos.DocumentScanner\\\" \

CONFIG += \
    auroraapp \
    auroraapp_i18n \

PKGCONFIG += \
    nemonotifications-qt5 \

INCLUDEPATH += \
    $$PWD/../libs/opencv/modules/core/include \
    $$PWD/../libs/opencv/modules/imgproc/include \
    $$OUT_PWD/../libs/opencv \

DEPENDPATH += \
    $$PWD/../libs/opencv/modules/core/include \
    $$PWD/../libs/opencv/modules/imgproc/include \
    $$OUT_PWD/../libs/opencv \

LIBS += \
    -L$$OUT_PWD/../libs/opencv/lib -lopencv_core -lopencv_imgproc

TRANSLATIONS += \
    translations/ru.auroraos.DocumentScanner.ts \
    translations/ru.auroraos.DocumentScanner-ru.ts \

HEADERS += \
    src/types.h \
    src/helper.h \
    src/cornerpoint.h \
    src/holedoverlay.h \
    src/photoeditor.h \
    src/photocamera.h \
    src/photorecognizer.h \
    src/photoaligner.h \
    src/page.h \
    src/pagemodel.h \
    src/document.h \
    src/documentmodel.h \
    src/storage.h \
    src/pdfwriter.h \
    src/service.h \

SOURCES += \
    src/main.cpp \
    src/types.cpp \
    src/helper.cpp \
    src/cornerpoint.cpp \
    src/holedoverlay.cpp \
    src/photoeditor.cpp \
    src/photocamera.cpp \
    src/photorecognizer.cpp \
    src/photoaligner.cpp \
    src/page.cpp \
    src/pagemodel.cpp \
    src/document.cpp \
    src/documentmodel.cpp \
    src/storage.cpp \
    src/pdfwriter.cpp \
    src/service.cpp \

DISTFILES += \
    qml/images/DocumentScanner.svg \
    qml/images/GraphicFingersensor.svg \
    qml/controls/PageSizeEditor.qml \
    qml/cover/DefaultCoverPage.qml \
    qml/pages/AboutPage.qml \
    qml/pages/DocumentSaverPage.qml \
    qml/pages/DocumentViewerPage.qml \
    qml/pages/PageViewerPage.qml \
    qml/pages/PageEditorPage.qml \
    qml/pages/PhotoShootingPage.qml \
    qml/DocumentScanner.qml \
    translations/*.ts \
    ru.auroraos.DocumentScanner.desktop \

include (src/utils/utils.pri)

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172
