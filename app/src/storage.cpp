/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#include <QtCore/QUuid>
#include <QtCore/QStandardPaths>

#include "storage.h"
#include "document.h"

DSStorage::DSStorage(QObject *parent)
    : QObject(parent), m_documentsPath(DSStorage::dataDir().absoluteFilePath(QStringLiteral("documents")))
{
    QStringList documentNames;
    QFile file(m_documentsPath);
    if (file.open(QFile::ReadOnly)) {
        QDataStream stream(&file);
        stream >> documentNames;
        file.close();
    }

    qint32 index = 0;
    for (const auto &documentName : documentNames)
        insertDocument(index++, new DSDocument(QUuid(documentName), this));
}

DSStorage::~DSStorage()
{
    QStringList pageNames;
    for (const auto &document : m_documents) {
        pageNames.append(document->id().toString());
        document->unload();
        delete document;
    }

    QFile file(m_documentsPath);
    if (file.open(QFile::WriteOnly | QFile::Truncate)) {
        QDataStream stream(&file);
        stream << pageNames;
        file.close();
    }
}

QDir DSStorage::dataDir()
{
    static const QDir dataDir(QStringLiteral("%1%2%3")
                              .arg(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation))
                              .arg(QDir::separator()).arg(QStringLiteral("data")));

    if (!dataDir.exists())
        dataDir.mkpath(dataDir.absolutePath());

    return dataDir;
}

qint32 DSStorage::documentsCount() const
{
    return m_documents.count();
}

qint32 DSStorage::indexOfDocument(DSDocument *document)
{
    return m_documents.indexOf(document);
}

DSDocument *DSStorage::document(qint32 index)
{
    if (index < 0 || index >= m_documents.count())
        return nullptr;

    return m_documents.at(index);
}

DSDocument *DSStorage::takeDocument(qint32 index)
{
    if (index < 0 || index >= m_documents.count())
        return nullptr;

    DSDocument *document = m_documents.takeAt(index);
    document->setParent(nullptr);

    emit documentTaken(index, document);
    emit documentsCountChanged(m_documents.count());

    return document;
}

void DSStorage::insertDocument(qint32 index, DSDocument *document)
{
    document->setParent(this);

    m_documents.insert(index, document);
    emit documentInserted(m_documents.indexOf(document), document);
    emit documentsCountChanged(m_documents.count());
}

void DSStorage::moveDocument(qint32 indexFrom, qint32 indexTo)
{
    if (indexFrom < 0 || indexFrom >= m_documents.size() || indexTo < 0 || indexTo >= m_documents.size() || indexFrom == indexTo)
        return;

    DSDocument *document = m_documents.at(indexFrom);
    if (!document)
        return;

    m_documents.move(indexFrom, indexTo);
    emit documentMoved(indexFrom, indexTo, document);
}
