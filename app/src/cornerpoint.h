/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef DSCORNERPOINT_H
#define DSCORNERPOINT_H

#include <QtQuick/QQuickItem>

class DSCornerPoint : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QPointF center READ center WRITE setCenter NOTIFY centerChanged)
    Q_PROPERTY(QColor fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)
    Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)
    Q_PROPERTY(qreal pointRadius READ pointRadius WRITE setPointRadius NOTIFY pointRadiusChanged)
    Q_PROPERTY(qreal captureRadius READ captureRadius WRITE setCaptureRadius NOTIFY captureRadiusChanged)

public:
    explicit DSCornerPoint(QQuickItem *parent = nullptr);

    QPointF center() const;
    void setCenter(const QPointF &center);

    QColor strokeColor() const;
    void setStrokeColor(const QColor &strokeColor);

    QColor fillColor() const;
    void setFillColor(const QColor &fillColor);

    qreal pointRadius() const;
    void setPointRadius(const qreal &pointRadius);

    qreal captureRadius() const;
    void setCaptureRadius(const qreal &captureRadius);

    bool captureRadiusVisible() const;
    void setCaptureRadiusVisible(bool captureRadiusVisible);

signals:
    void centerChanged(const QPointF &center);
    void strokeColorChanged(const QColor &strokeColor);
    void fillColorChanged(const QColor &fillColor);
    void pointRadiusChanged(const qreal &pointRadius);
    void captureRadiusChanged(const qreal &captureRadius);
    void captureRadiusVisibleChanged(bool captureRadiusVisible);

private slots:
    void _updateSize();

protected:
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *data) override;

private:
    QColor m_strokeColor { QColor(50, 50, 50) };
    QColor m_fillColor { QColor(255, 255, 255) };
    qreal m_pointRadius { 8.0f };
    qreal m_captureRadius { 100.0f };
    bool m_captureRadiusVisible { true };
};

Q_DECLARE_METATYPE(DSCornerPoint *)

#endif // DSCORNERPOINT_H
