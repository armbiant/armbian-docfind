/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#include "pagemodel.h"
#include "page.h"
#include "document.h"

enum PMDataRole : qint32 {
    ImageRole = Qt::UserRole + 1,
    AlignedImageRole,
    PreviewImageRole,
};

DSPageModel::DSPageModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int DSPageModel::rowCount(const QModelIndex &index) const
{
    Q_UNUSED(index)

    if (!m_document)
        return 0;

    return m_document->pagesCount();
}

QVariant DSPageModel::data(const QModelIndex &index, int role) const
{
    if (!m_document || !index.isValid() || index.row() < 0 || index.row() >= m_document->pagesCount())
        return QVariant();

    const DSPage *page = m_document->page(index.row());

    switch (role) {
    case ImageRole:
        return QVariant::fromValue(page->imagePath());
    case AlignedImageRole:
        return QVariant::fromValue(page->alignedImagePath());
    case PreviewImageRole:
        return QVariant::fromValue(page->previewImagePath());
    }

    return QVariant();
}

QHash<int, QByteArray> DSPageModel::roleNames() const
{
    static QHash<int, QByteArray> roles = {
        { ImageRole, "image" },
        { AlignedImageRole, "alignedImage" },
        { PreviewImageRole, "previewImage" },
    };

    return roles;
}

DSDocument *DSPageModel::document() const
{
    return m_document;
}

void DSPageModel::setDocument(DSDocument *document)
{
    if (m_document == document)
        return;

    if (m_document) {
        disconnect(m_document);
        for (qint32 i = 0; i < m_document->pagesCount(); ++i)
            disconnect(m_document->page(i));
    }

    m_document = document;

    if (m_document) {
        _connectToDocument(m_document);
        for (qint32 i = 0; i < m_document->pagesCount(); ++i)
            _connectToPage(m_document->page(i));
    }

    emit documentChanged(m_document);
}

void DSPageModel::_connectToDocument(DSDocument *document)
{
    if (!document)
        return;

    connect(document, &DSDocument::pageInserted, this, [this](qint32 index, DSPage *page) {
        _connectToPage(page);
        beginInsertRows(QModelIndex(), index, index);
        endInsertRows();
    });
    connect(document, &DSDocument::pageTaken, this, [this](qint32 index, DSPage *page) {
        disconnect(page);
        beginRemoveRows(QModelIndex(), index, index);
        endRemoveRows();
    });
    connect(document, &DSDocument::pageMoved, this, [this](qint32 indexFrom, qint32 indexTo, DSPage *page) {
        Q_UNUSED(page)

        beginMoveRows(QModelIndex(), indexFrom, indexFrom, QModelIndex(), indexTo > indexFrom ? indexTo + 1 : indexTo);
        endMoveRows();
    });
}

void DSPageModel::_connectToPage(DSPage *page)
{
    if (!page)
        return;

    connect(page, &DSPage::imagePathChanged, this, [this]() {
        _updateModel(sender(), ImageRole);
    });
    connect(page, &DSPage::alignedImagePathChanged, this, [this]() {
        _updateModel(sender(), AlignedImageRole);
    });
    connect(page, &DSPage::previewImagePathChanged, this, [this]() {
        _updateModel(sender(), PreviewImageRole);
    });
}

void DSPageModel::_updateModel(QObject *sender, qint32 dataRole)
{
    auto modelIndex = [this](QObject *sender) {
        DSPage *page = qobject_cast<DSPage *>(sender);
        if (!page)
            return QModelIndex();

        qint32 index = m_document->indexOfPage(page);
        if (index == -1)
            return QModelIndex();

        return this->index(index);
    };

    QModelIndex index = modelIndex(sender);
    emit dataChanged(index, index, { dataRole });
}
