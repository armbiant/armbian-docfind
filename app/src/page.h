/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef DSPAGE_H
#define DSPAGE_H

#include <QtCore/QObject>
#include <QtCore/QUuid>
#include <QtCore/QFutureWatcher>

#include "types.h"

class DSPage : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString imagePath READ imagePath NOTIFY imagePathChanged)
    Q_PROPERTY(QString alignedImagePath READ alignedImagePath NOTIFY alignedImagePathChanged)
    Q_PROPERTY(QString previewImagePath READ previewImagePath NOTIFY previewImagePathChanged)
    Q_PROPERTY(DSFrame frame READ frame WRITE setFrame NOTIFY frameChanged)
    Q_PROPERTY(bool cache READ cache WRITE setCache NOTIFY cacheChanged)
    Q_PROPERTY(bool savingActive READ savingActive NOTIFY savingActiveChanged)
    Q_PROPERTY(bool loadingActive READ loadingActive NOTIFY loadingActiveChanged)

public:
    explicit DSPage(QObject *parent = nullptr);
    DSPage(const QUuid &id, QObject *parent = nullptr);
    ~DSPage() override;

    QUuid id() const;

    QString imagePath() const;
    QString alignedImagePath() const;
    QString previewImagePath() const;

    DSFrame frame() const;
    void setFrame(const DSFrame &frame);

    bool cache() const;
    void setCache(bool cache);

    bool savingActive() const;
    bool loadingActive() const;

    Q_INVOKABLE void remove();

signals:
    void imagePathChanged(const QString &imagePath);
    void alignedImagePathChanged(const QString &alignedImagePath);
    void previewImagePathChanged(const QString &previewImagePath);
    void frameChanged(const DSFrame &frame);
    void cacheChanged(bool cache);
    void savingActiveChanged(bool savingActive);
    void loadingActiveChanged(bool loadingActive);

private slots:
    void _tryProcess();
    void _processSave(const DSFrame &frame) const;
    DSFrame _processLoad() const;

private:
    const QUuid m_id {  };
    const QString m_imagePath {  };
    const QString m_alignedImagePath {  };
    const QString m_pointsPath {  };

    QString m_imagePathCur {  };
    QString m_alignedImagePathCur {  };
    QString m_previewImagePathCur {  };

    bool m_removeRequest { false };
    bool m_loadRequest { false };
    bool m_cache { false };
    bool m_cacheUpdated { false };
    DSFrame m_frameCache {  };
    DSFrame m_frameSave {  };
    QFutureWatcher<void> m_saveThread {  };
    QFutureWatcher<DSFrame> m_loadThread {  };
};

Q_DECLARE_METATYPE(DSPage *)

#endif // DSPAGE_H
