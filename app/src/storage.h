/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef DSSTORAGE_H
#define DSSTORAGE_H

#include <QtCore/QObject>
#include <QtCore/QDir>

#include "types.h"

class DSDocument;

class DSStorage : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint32 documentsCount READ documentsCount NOTIFY documentsCountChanged)

public:
    explicit DSStorage(QObject *parent = nullptr);
    ~DSStorage() override;

    static QDir dataDir();

    qint32 documentsCount() const;

    Q_INVOKABLE qint32 indexOfDocument(DSDocument *document);
    Q_INVOKABLE DSDocument *document(qint32 index);
    Q_INVOKABLE DSDocument *takeDocument(qint32 index);
    Q_INVOKABLE void insertDocument(qint32 index, DSDocument *document);
    Q_INVOKABLE void moveDocument(qint32 indexFrom, qint32 indexTo);

signals:
    void documentsCountChanged(qint32 documentsCount);
    void documentInserted(qint32 index, DSDocument *document);
    void documentMoved(qint32 indexFrom, qint32 indexTo, DSDocument *document);
    void documentTaken(qint32 index, DSDocument *document);

private:
    const QString m_documentsPath {  };

    QList<DSDocument *> m_documents {  };
};

Q_DECLARE_METATYPE(DSStorage *)

#endif // DSSTORAGE_H
