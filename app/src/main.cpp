/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickView>
#include <QtQml/QQmlContext>
#include <auroraapp.h>

#include "types.h"
#include "helper.h"
#include "photoeditor.h"
#include "photocamera.h"
#include "photorecognizer.h"
#include "photoaligner.h"
#include "page.h"
#include "pagemodel.h"
#include "document.h"
#include "documentmodel.h"
#include "storage.h"
#include "pdfwriter.h"
#include "service.h"

template <typename T>
static QObject *singletonCreate(QQmlEngine *qmlEngine, QJSEngine *jsEngine)
{
    Q_UNUSED(qmlEngine)
    Q_UNUSED(jsEngine)

    return new T;
}

int main(int argc, char *argv[])
{
    qmlRegisterSingletonType<DSHelper>("ru.auroraos.DocumentScanner", 1, 0, "DSHelper", singletonCreate<DSHelper>);
    qmlRegisterUncreatableType<DSPageSize>("ru.auroraos.DocumentScanner", 1, 0, "DSPageSize", "");
    qmlRegisterUncreatableType<DSFrame>("ru.auroraos.DocumentScanner", 1, 0, "DSFrame", "");
    qmlRegisterType<DSPhotoCamera>("ru.auroraos.DocumentScanner", 1, 0, "DSPhotoCamera");
    qmlRegisterType<DSPhotoRecognizer>("ru.auroraos.DocumentScanner", 1, 0, "DSPhotoRecognizer");
    qmlRegisterType<DSPhotoAligner>("ru.auroraos.DocumentScanner", 1, 0, "DSPhotoAligner");
    qmlRegisterType<DSPhotoEditor>("ru.auroraos.DocumentScanner", 1, 0, "DSPhotoEditor");
    qmlRegisterType<DSPage>("ru.auroraos.DocumentScanner", 1, 0, "DSPage");
    qmlRegisterType<DSPageModel>("ru.auroraos.DocumentScanner", 1, 0, "DSPageModel");
    qmlRegisterType<DSDocument>("ru.auroraos.DocumentScanner", 1, 0, "DSDocument");
    qmlRegisterType<DSDocumentModel>("ru.auroraos.DocumentScanner", 1, 0, "DSDocumentModel");

    qRegisterMetaType<DSPageSize>("DSPageSize");
    qRegisterMetaType<DSPageSize::Id>("DSPageSize::Id");
    qRegisterMetaType<DSFrame>("DSFrame");
    qRegisterMetaType<DSPage *>("DSPage *");
    qRegisterMetaType<DSPageModel *>("DSPageModel *");
    qRegisterMetaType<DSDocument *>("DSDocument *");
    qRegisterMetaType<DSDocumentModel *>("DSDocumentModel *");
    qRegisterMetaType<DSStorage *>("DSStorage *");
    qRegisterMetaType<DSPdfWriter *>("DSPdfWriter *");

    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("DocumentScanner"));

    QStringList applicationArgs = application->arguments();
    applicationArgs.removeFirst();

    if (!applicationArgs.isEmpty()) {
        QScopedPointer<DSService> service(new DSService(application.data()));

        QObject::connect(service.data(), &DSService::fileOpened,
                         application.data(), &QGuiApplication::quit);

        service->openFile(applicationArgs.first());

        return application->exec();
    } else {
        QScopedPointer<DSService> service(new DSService(application.data()));
        QScopedPointer<DSStorage> storage(new DSStorage());
        QScopedPointer<DSPdfWriter> pdfWriter(new DSPdfWriter());
        QScopedPointer<QQuickView> view(Aurora::Application::createView());
        QSurfaceFormat format = view->format();
        format.setSamples(16);
        view->setFormat(format);
        view->rootContext()->setContextProperty(QStringLiteral("Storage"), storage.data());
        view->rootContext()->setContextProperty(QStringLiteral("PdfWriter"), pdfWriter.data());
        view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/DocumentScanner.qml")));
        view->show();

        return application->exec();
    }
}
