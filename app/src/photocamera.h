/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef DSPHOTOCAMERA_H
#define DSPHOTOCAMERA_H

#include <QtCore/QSize>
#include <QtGui/QTransform>
#include <QtMultimedia/QCameraInfo>
#include <QtMultimedia/QAbstractVideoSurface>

#include "types.h"

class QCameraImageCapture;
class QMetaDataWriterControl;

class DSPhotoCamera : public QAbstractVideoSurface
{
    Q_OBJECT

    Q_PROPERTY(DSFrame::ImageType frameType READ frameType NOTIFY frameTypeChanged)
    Q_PROPERTY(DSFrame frame READ frame NOTIFY frameChanged)

public:
    explicit DSPhotoCamera(QObject *parent = nullptr);
    ~DSPhotoCamera() override;

    Q_INVOKABLE void catchPhoto();
    Q_INVOKABLE void clearPhoto();

    DSFrame::ImageType frameType() const;
    DSFrame frame() const;

signals:
    void frameTypeChanged(const DSFrame::ImageType &frameType);
    void frameChanged(const DSFrame &frame);

protected:
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const override;
    bool present(const QVideoFrame &frame) override;

private:
    QCamera *m_camera { nullptr };
    QCameraImageCapture *m_cameraCapture { nullptr };
    QMetaDataWriterControl *m_metaDataControl { nullptr };
    QCameraInfo m_cameraInfo {  };
    QTransform m_transform {  };
    QSize m_streamResolution {  };
    QSize m_screenResolution {  };
    bool m_resolutionsIsSet { false };
    DSFrame m_frame {  };
};

Q_DECLARE_METATYPE(DSPhotoCamera *)

#endif // DSPHOTOCAMERA_H
