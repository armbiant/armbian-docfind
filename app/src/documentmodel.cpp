/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#include "documentmodel.h"
#include "document.h"
#include "storage.h"

enum DMDataRole : qint32 {
    PreviewRole = Qt::UserRole + 1,
    NameRole,
};

DSDocumentModel::DSDocumentModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int DSDocumentModel::rowCount(const QModelIndex &index) const
{
    Q_UNUSED(index)

    if (!m_storage)
        return 0;

    return m_storage->documentsCount();
}

QVariant DSDocumentModel::data(const QModelIndex &index, int role) const
{
    if (!m_storage || !index.isValid() || index.row() < 0 || index.row() >= m_storage->documentsCount())
        return QVariant();

    const DSDocument *document = m_storage->document(index.row());

    switch (role) {
    case PreviewRole:
        return QVariant::fromValue(document->preview());
    case NameRole:
        return QVariant::fromValue(document->name());
    }

    return QVariant();
}

QHash<int, QByteArray> DSDocumentModel::roleNames() const
{
    static QHash<int, QByteArray> roles = {
        { PreviewRole, "preview" },
        { NameRole, "name" }
    };

    return roles;
}

DSStorage *DSDocumentModel::storage() const
{
    return m_storage;
}

void DSDocumentModel::setStorage(DSStorage *storage)
{
    if (m_storage == storage)
        return;

    if (m_storage) {
        disconnect(m_storage);
        for (qint32 i = 0; i < m_storage->documentsCount(); ++i)
            disconnect(m_storage->document(i));
    }

    m_storage = storage;

    if (m_storage) {
        _connectToDocuments(m_storage);
        for (qint32 i = 0; i < m_storage->documentsCount(); ++i)
            _connectToDocument(m_storage->document(i));
    }

    emit storageChanged(m_storage);
}

void DSDocumentModel::_connectToDocuments(DSStorage *storage)
{
    if (!storage)
        return;

    connect(storage, &DSStorage::documentInserted, this, [this](qint32 index, DSDocument *document) {
        _connectToDocument(document);
        beginInsertRows(QModelIndex(), index, index);
        endInsertRows();
    });
    connect(storage, &DSStorage::documentTaken, this, [this](qint32 index, DSDocument *document) {
        disconnect(document);
        beginRemoveRows(QModelIndex(), index, index);
        endRemoveRows();
    });
    connect(storage, &DSStorage::documentMoved, this, [this](qint32 indexFrom, qint32 indexTo, DSDocument *document) {
        Q_UNUSED(document)

        beginMoveRows(QModelIndex(), indexFrom, indexFrom, QModelIndex(), indexTo > indexFrom ? indexTo + 1 : indexTo);
        endMoveRows();
    });
}

void DSDocumentModel::_connectToDocument(DSDocument *document)
{
    if (!document)
        return;

    connect(document, &DSDocument::previewChanged, this, [this]() {
        _updateModel(sender(), PreviewRole);
    });
    connect(document, &DSDocument::nameChanged, this, [this]() {
        _updateModel(sender(), NameRole);
    });
}

void DSDocumentModel::_updateModel(QObject *sender, qint32 dataRole)
{
    auto modelIndex = [this](QObject *sender) {
        DSDocument *document = qobject_cast<DSDocument *>(sender);
        if (!document)
            return QModelIndex();

        qint32 index = m_storage->indexOfDocument(document);
        if (index == -1)
            return QModelIndex();

        return this->index(index);
    };

    QModelIndex index = modelIndex(sender);
    emit dataChanged(index, index, { dataRole });
}
