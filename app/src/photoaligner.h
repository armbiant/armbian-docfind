/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef DSPHOTOALIGNER_H
#define DSPHOTOALIGNER_H

#include <QtCore/QObject>
#include <QtCore/QFutureWatcher>

#include "types.h"

class DSPhotoAligner : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool processing READ processing NOTIFY processingChanged)
    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY activeChanged)
    Q_PROPERTY(DSFrame::ImageType frameType READ frameType NOTIFY frameTypeChanged)
    Q_PROPERTY(DSFrame frame READ frame WRITE setFrame NOTIFY frameChanged)

public:
    explicit DSPhotoAligner(QObject *parent = nullptr);
    ~DSPhotoAligner() override;

    bool processing() const;
    bool active() const;
    void setActive(bool active);

    DSFrame::ImageType frameType() const;
    DSFrame frame() const;
    void setFrame(const DSFrame &frame);

signals:
    void processingChanged(bool processing);
    void activeChanged(bool active);
    void frameTypeChanged(const DSFrame::ImageType &frameType);
    void frameChanged(const DSFrame &frame);

private:
    void _startThread(const DSFrame &frame);

private:
    bool m_active { false };
    DSFrame m_frame {  };
    DSFrame m_frameCache {  };

    QFutureWatcher<QImage> m_processingThread {  };
};

Q_DECLARE_METATYPE(DSPhotoAligner *)

#endif // DSPHOTOALIGNER_H
