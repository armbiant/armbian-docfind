/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#include <QtConcurrent/QtConcurrent>

#include "photoaligner.h"
#include "imageproc.h"

DSPhotoAligner::DSPhotoAligner(QObject *parent)
    : QObject(parent)
{
    connect(&m_processingThread, &QFutureWatcherBase::started, this, [this]() {
        emit processingChanged(true);
    });
    connect(&m_processingThread, &QFutureWatcherBase::finished, this, [this]() {
        emit processingChanged(false);

        m_frame.alignedImage = m_processingThread.result();
        emit frameTypeChanged(m_frame.imageType);
        emit frameChanged(m_frame);

        if (m_frameCache != DSFrame()) {
            if (m_frameCache.image != m_frame.image && m_frameCache.points != m_frame.points)
                _startThread(m_frameCache);
            m_frameCache = DSFrame();
        }
    });
}

DSPhotoAligner::~DSPhotoAligner()
{
    m_processingThread.waitForFinished();
}

bool DSPhotoAligner::processing() const
{
    return !m_processingThread.isFinished();
}

bool DSPhotoAligner::active() const
{
    return m_active;
}

void DSPhotoAligner::setActive(bool active)
{
    if (m_active == active)
        return;

    m_active = active;
    emit activeChanged(active);

    _startThread(m_frame);
}

DSFrame::ImageType DSPhotoAligner::frameType() const
{
    return m_frame.imageType;
}

DSFrame DSPhotoAligner::frame() const
{
    return m_frame;
}

void DSPhotoAligner::setFrame(const DSFrame &frame)
{
    if (m_frame == frame)
        return;

    m_frameCache = frame;
    m_frame = frame;
    emit frameTypeChanged(frame.imageType);
    emit frameChanged(frame);

    _startThread(frame);
}

void DSPhotoAligner::_startThread(const DSFrame &frame)
{
    if (!m_active || !m_processingThread.isFinished())
        return;

    const QImage &image = frame.image;
    const QVector<QPointF> &points = frame.points;
    m_processingThread.setFuture(QtConcurrent::run(&ImageProc::alignPerspective, image, points));
}
