/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#include <QtCore/QHash>

#include "types.h"

const QHash<DSPageSize::Id, QSize> pageSizes = {
    { DSPageSize::A0, QSize(841, 1189) },
    { DSPageSize::A1, QSize(594, 841) },
    { DSPageSize::A2, QSize(420, 594) },
    { DSPageSize::A3, QSize(297, 420) },
    { DSPageSize::A4, QSize(210, 297) },
    { DSPageSize::A5, QSize(148, 210) },
    { DSPageSize::A6, QSize(105, 148) },
};

DSPageSize::DSPageSize()
    : m_sizeId(Custom), m_sizeMm(QSize(1, 1))
{
}

DSPageSize::DSPageSize(Id sizeId)
    : m_sizeId(sizeId), m_sizeMm(pageSizes.value(sizeId, QSize(1, 1)))
{
}

DSPageSize::DSPageSize(const QSize &sizeMm)
    : m_sizeId(pageSizes.key(sizeMm, Custom)), m_sizeMm(sizeMm)
{
}

void DSPageSize::setSizeId(Id sizeId)
{
    m_sizeId = sizeId;
    m_sizeMm = pageSizes.value(sizeId, m_sizeMm);
}

DSPageSize::Id DSPageSize::sizeId() const
{
    return m_sizeId;
}

void DSPageSize::setSizeMm(const QSize &sizeMm)
{
    m_sizeId = pageSizes.key(sizeMm, Custom);
    m_sizeMm = sizeMm;
}

QSize DSPageSize::sizeMm() const
{
    return m_sizeMm;
}

bool operator==(const DSPageSize &left, const DSPageSize &right)
{
    return left.m_sizeId == right.m_sizeId && left.m_sizeMm == right.m_sizeMm;
}

bool operator!=(const DSPageSize &left, const DSPageSize &right)
{
    return !(left == right);
}
