/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#include <QtCore/QFile>
#include <QtGui/QScreen>
#include <QtGui/QGuiApplication>
#include <QtMultimedia/QCameraImageCapture>
#include <QtMultimedia/QMetaDataWriterControl>

#include "photocamera.h"
#include "imageproc.h"

QList<QSize> filterResolutions(const QList<QSize> &resolutions, quint64 minArea, quint64 maxArea, qreal ratio)
{
    enum : qint32 {
        Ratio = 0,
        Area = 1,
        Size = 2,
    };

    auto makeTuple = [](qreal ratio, quint64 area, const QSize &size) {
        return std::make_tuple<>(ratio, area, size);
    };

    auto makeTupleList = [makeTuple](const QList<QSize> &sizes) {
        QList<std::tuple<qreal, quint64, QSize>> tuples;
        for (const auto &size : sizes) {
            const quint64 area = size.width() * size.height();
            const qreal ratio = static_cast<qreal>(size.width()) / static_cast<qreal>(size.height());
            tuples.append(makeTuple(ratio, area, size));
        }

        return tuples;
    };

    auto makeSizeList = [](const QList<std::tuple<qreal, quint64, QSize>> &tuple) {
        QList<QSize> sizes;
        for (const auto item : tuple)
            sizes.append(std::get<Size>(item));

        return sizes;
    };

    auto filterTupleListByArea = [](QList<std::tuple<qreal, quint64, QSize>> &tuple, quint64 min, quint64 max) {
        for (const auto item : tuple) {
            if (std::get<Area>(item) >= min && std::get<Area>(item) <= max)
                continue;
            tuple.removeOne(item);
        }
    };

    auto filterTupleListByRatio = [](QList<std::tuple<qreal, quint64, QSize>> &tuple, qreal bestRatio) {
        qreal delta = std::numeric_limits<qreal>::max();
        qreal ratio = 0.0f;

        for (const auto item : tuple) {
            const qreal itemRatio = std::get<Ratio>(item);
            const qreal itemDelta = qAbs(itemRatio - bestRatio);
            if (itemDelta < delta) {
                delta = itemDelta;
                ratio = itemRatio;
            }
        }

        for (const auto item : tuple) {
            if (std::get<Ratio>(item) == ratio)
                continue;
            tuple.removeOne(item);
        }
    };

    auto resolutionTuples = makeTupleList(resolutions);
    filterTupleListByArea(resolutionTuples, minArea, maxArea);
    filterTupleListByRatio(resolutionTuples, ratio);

    return makeSizeList(resolutionTuples);
};

DSPhotoCamera::DSPhotoCamera(QObject *parent)
    : QAbstractVideoSurface(parent)
{
    m_camera = new QCamera(QCamera::BackFace, this);
    m_camera->setCaptureMode(QCamera::CaptureStillImage);
    m_camera->focus()->setFocusMode(QCameraFocus::ContinuousFocus);
    m_camera->exposure()->setFlashMode(QCameraExposure::FlashOff);
    m_camera->setViewfinder(this);

    m_cameraInfo = QCameraInfo(*m_camera);

    m_cameraCapture = new QCameraImageCapture(m_camera, this);
    m_cameraCapture->setCaptureDestination(QCameraImageCapture::CaptureToBuffer | QCameraImageCapture::CaptureToFile);

    m_metaDataControl = m_camera->service() ? m_camera->service()->requestControl<QMetaDataWriterControl *>() : nullptr;

    connect(m_camera, &QCamera::statusChanged, this, [this](QCamera::Status status) {
        if (status == QCamera::LoadedStatus && !m_resolutionsIsSet) {
            constexpr quint64 videoResMin = 960 * 720; // 640 * 480
            constexpr quint64 videoResMax = 1440 * 1080;
            constexpr quint64 photoResMin = 1440 * 1080;
            constexpr quint64 photoResMax = 3264 * 2448;
            constexpr qreal pageA4Ratio = 297.0f / 210.0f;

            const QList<QSize> streamResolutions = filterResolutions(m_camera->supportedViewfinderResolutions(),
                                                                     videoResMin, videoResMax, pageA4Ratio);
            const QList<QSize> screenResolutions = filterResolutions(m_cameraCapture->supportedResolutions(),
                                                                     photoResMin, photoResMax, pageA4Ratio);

            if (!streamResolutions.isEmpty() && !screenResolutions.isEmpty()) {
                m_streamResolution = streamResolutions.first();
                m_screenResolution = screenResolutions.last();
                m_camera->unload();
            }
        } else if (status == QCamera::UnloadedStatus && !m_resolutionsIsSet && !m_streamResolution.isEmpty()) {
            QCameraViewfinderSettings viefinderSettings = m_camera->viewfinderSettings();
            viefinderSettings.setResolution(m_streamResolution);
            m_camera->setViewfinderSettings(viefinderSettings);

            QImageEncoderSettings encodingSettings = m_cameraCapture->encodingSettings();
            encodingSettings.setResolution(m_screenResolution);
            m_cameraCapture->setEncodingSettings(encodingSettings);

            m_resolutionsIsSet = true;
            m_camera->start();
        }
    });

    connect(m_cameraCapture, &QCameraImageCapture::imageAvailable, this, [this](qint32 id, const QVideoFrame &frame) {
        Q_UNUSED(id)

        m_frame.image = ImageProc::imageFromFrame(frame).transformed(m_transform);
        m_frame.imageType = DSFrame::Photo;
        emit frameTypeChanged(m_frame.imageType);
        emit frameChanged(m_frame);
    });
    connect(m_cameraCapture, &QCameraImageCapture::imageSaved, this, [this](qint32 id, const QString &fileName) {
        Q_UNUSED(id)

        if (QFile::exists(fileName))
            QFile::remove(fileName);
    });

    m_camera->start();

    QScreen *screen = QGuiApplication::primaryScreen();
    screen->setOrientationUpdateMask(Qt::PortraitOrientation
                                     | Qt::LandscapeOrientation
                                     | Qt::InvertedPortraitOrientation
                                     | Qt::InvertedLandscapeOrientation);

    auto updateTransform = [this, screen](Qt::ScreenOrientation orientation) {
        const qint32 screenRotation = (360 - screen->angleBetween(screen->nativeOrientation(), orientation)) % 360;
        const qint32 cameraRotation = m_cameraInfo.orientation();
        const qint32 rotation = (720 + cameraRotation + screenRotation) % 360;

        QTransform transform;
        transform.rotate(-cameraRotation);
        m_transform = transform;

        if (m_metaDataControl)
            m_metaDataControl->setMetaData(QStringLiteral("Orientation"), rotation);
    };

    connect(screen, &QScreen::orientationChanged,
            this, updateTransform);

    updateTransform(screen->orientation());
}

DSPhotoCamera::~DSPhotoCamera()
{
    m_camera->stop();
}

void DSPhotoCamera::catchPhoto()
{
    m_cameraCapture->cancelCapture();
    m_cameraCapture->capture();
}

void DSPhotoCamera::clearPhoto()
{
    m_cameraCapture->cancelCapture();
    m_frame.image = QImage();
    m_frame.imageType = DSFrame::Unknown;
}

DSFrame::ImageType DSPhotoCamera::frameType() const
{
    return m_frame.imageType;
}

DSFrame DSPhotoCamera::frame() const
{
    return m_frame;
}

QList<QVideoFrame::PixelFormat> DSPhotoCamera::supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const
{
    Q_UNUSED(handleType)

    return QList<QVideoFrame::PixelFormat>() << QVideoFrame::Format_YV12 << QVideoFrame::Format_NV12 << QVideoFrame::Format_NV21;
}

bool DSPhotoCamera::present(const QVideoFrame &frame)
{
    if (m_frame.imageType == DSFrame::Photo)
        return true;

    m_frame.image = ImageProc::imageFromFrame(frame).transformed(m_transform);
    m_frame.imageType = DSFrame::Video;
    emit frameTypeChanged(m_frame.imageType);
    emit frameChanged(m_frame);

    return true;
}
