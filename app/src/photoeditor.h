/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef DSPHOTOEDITOR_H
#define DSPHOTOEDITOR_H

#include <QtQuick/QQuickItem>

#include "types.h"

class QSGSimpleTextureNode;
class DSHoledOverlay;
class DSCornerPoint;

class DSPhotoEditor : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(FillMode fillMode READ fillMode WRITE setFillMode NOTIFY fillModeChanged)
    Q_PROPERTY(DSFrame frame READ frame WRITE setFrame NOTIFY frameChanged)
    Q_PROPERTY(QColor overlayColor READ overlayColor WRITE setOverlayColor NOTIFY overlayColorChanged)
    Q_PROPERTY(QColor pointsColor READ pointsColor WRITE setPointsColor NOTIFY pointsColorChanged)
    Q_PROPERTY(QColor pointsHighlightedColor READ pointsHighlightedColor WRITE setPointsHighlightedColor NOTIFY pointsHighlightedColorChanged)
    Q_PROPERTY(bool pointsVisible READ pointsVisible WRITE setPointsVisible NOTIFY pointsVisibleChanged)
    Q_PROPERTY(bool pointsManualMoving READ pointsManualMoving WRITE setPointsManualMoving NOTIFY pointsManualMovingChanged)
    Q_PROPERTY(bool pointIsCaptured READ pointIsCaptured NOTIFY pointIsCapturedChanged)
    Q_PROPERTY(QRectF imageFilledRect READ imageFilledRect NOTIFY imageFilledRectChanged)

public:
    enum FillMode : qint32 {
        Stretch,
        PreserveAspectFit,
        PreserveAspectCrop,
    };
    Q_ENUM(FillMode)

public:
    explicit DSPhotoEditor(QQuickItem *parent = nullptr);

    Q_INVOKABLE void rotateLeft();
    Q_INVOKABLE void rotateRight();

    FillMode fillMode() const;
    void setFillMode(FillMode fillMode);

    DSFrame frame() const;
    void setFrame(const DSFrame &frame);

    QColor overlayColor() const;
    void setOverlayColor(const QColor &overlayColor);

    QColor pointsColor() const;
    void setPointsColor(const QColor &pointsColor);

    QColor pointsHighlightedColor() const;
    void setPointsHighlightedColor(const QColor &pointsHighlightedColor);

    bool pointsVisible() const;
    void setPointsVisible(bool pointsVisible);

    bool pointsManualMoving() const;
    void setPointsManualMoving(bool pointsManualMoving);

    bool pointIsCaptured() const;
    QRectF imageFilledRect() const;

signals:
    void fillModeChanged(FillMode fillMode);
    void frameChanged(const DSFrame &frame);
    void overlayColorChanged(const QColor &overlayColor);
    void pointsColorChanged(const QColor &pointsColor);
    void pointsHighlightedColorChanged(const QColor &pointsHighlightedColor);
    void pointsVisibleChanged(bool pointsVisible);
    void pointsManualMovingChanged(bool pointsManualMoving);
    void pointIsCapturedChanged(bool pointIsCaptured);
    void imageFilledRectChanged(const QRectF &imageFilledRect);

private slots:
    void _rotateFrame(const qreal &angle);
    void _updateFrame(const DSFrame &frame);
    void _updateRects();

protected:
    bool eventFilter(QObject *target, QEvent *event) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *data) override;

private:
    static qint32 m_maxTextureSize;

    FillMode m_fillMode { FillMode::PreserveAspectFit };
    DSFrame m_frame {  };
    QRectF m_rect {  };
    QRectF m_imageRect {  };
    QRectF m_imageRectScaled {  };
    QColor m_pointsColor { QColor(255, 255, 255) };
    QColor m_pointsHighlightedColor { QColor(200, 200, 100) };
    bool m_pointsVisible { false };
    bool m_pointsManualMoving { false };
    bool m_pointIsCaptured { false };

    DSHoledOverlay *m_holedOverlayItem { nullptr };
    QList<DSCornerPoint *> m_cornerPointItemList {  };
};

Q_DECLARE_METATYPE(DSPhotoEditor *)

#endif // DSPHOTOEDITOR_H
