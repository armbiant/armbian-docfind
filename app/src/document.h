/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

#ifndef DSDOCUMENT_H
#define DSDOCUMENT_H

#include <QtCore/QObject>
#include <QtCore/QUuid>

class DSPage;

class DSDocument : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString preview READ preview NOTIFY previewChanged)
    Q_PROPERTY(qint32 pagesCount READ pagesCount NOTIFY pagesCountChanged)

public:
    explicit DSDocument(QObject *parent = nullptr);
    DSDocument(const QUuid &id, QObject *parent = nullptr);
    ~DSDocument() override;

    QUuid id() const;

    QString name() const;
    void setName(const QString &name);

    QString preview() const;

    qint32 pagesCount() const;

    Q_INVOKABLE void remove();
    Q_INVOKABLE void unload();
    Q_INVOKABLE void upload();

    Q_INVOKABLE qint32 indexOfPage(DSPage *page);
    Q_INVOKABLE DSPage *page(qint32 index);
    Q_INVOKABLE DSPage *takePage(qint32 index);
    Q_INVOKABLE void insertPage(qint32 index, DSPage *page);
    Q_INVOKABLE void movePage(qint32 indexFrom, qint32 indexTo);

signals:
    void nameChanged(const QString &name);
    void previewChanged(const QString &preview);
    void pagesCountChanged(qint32 pagesCount);
    void pageInserted(qint32 index, DSPage *page);
    void pageMoved(qint32 indexFrom, qint32 indexTo, DSPage *page);
    void pageTaken(qint32 index, DSPage *page);

private slots:
    void _updatePreview();

private:
    const QUuid m_id {  };
    const QString m_pagesPath {  };

    QString m_name {  };
    QString m_previewPath {  };
    QList<DSPage *> m_pages {  };
};

Q_DECLARE_METATYPE(DSDocument *)

#endif // DSDOCUMENT_H
