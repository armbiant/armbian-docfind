/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.DocumentScanner 1.0

Item {
    id: pageSizeEditor

    property var pageSize
    property bool settingsIsValid: !pageWidthMmEdit.errorHighlight && !pageHeightMmEdit.errorHighlight

    objectName: "pageSizeEditor"
    anchors {
        left: parent.left
        right: parent.right
    }
    height: pageSizeColumn.implicitHeight

    Column {
        id: pageSizeColumn

        property var pageSize
        property int pageSizeId
        property size pageSizeMm

        objectName: "pageSizeColumn"
        anchors {
            left: parent.left
            right: parent.right
        }
        spacing: Theme.paddingSmall

        onPageSizeIdChanged: {
            pageSizeColumn.pageSize.sizeId = pageSizeColumn.pageSizeId;
            pageSizeColumn.pageSizeMm = pageSizeColumn.pageSize.sizeMm;
            pageSizeEditor.pageSize = pageSizeColumn.pageSize;
            pageSizeIdCmbBox.currentIndex = pageSizeColumn.pageSizeId;
        }
        onPageSizeMmChanged: {
            pageSizeColumn.pageSize.sizeMm = pageSizeColumn.pageSizeMm;
            pageSizeColumn.pageSizeId = pageSizeColumn.pageSize.sizeId;
            pageSizeEditor.pageSize = pageSizeColumn.pageSize;
        }
        Component.onCompleted: {
            if (pageSizeEditor.pageSize === undefined)
                pageSizeColumn.pageSize = DSHelper.pageSize(DSPageSize.A4);
            else
                pageSizeColumn.pageSize = pageSizeEditor.pageSize;

            pageSizeColumn.pageSizeId = pageSizeColumn.pageSize.sizeId;
            pageSizeColumn.pageSizeMm = pageSizeColumn.pageSize.sizeMm;
        }

        ComboBox {
            id: pageSizeIdCmbBox

            objectName: "pageSizeIdCmbBox"
            width: parent.width
            label: qsTr("Page size:")
            description: qsTr("Select the desired page size ID")
            currentIndex: pageSizeColumn.pageSizeId
            menu: ContextMenu {
                Repeater {
                    model: ListModel {
                        id: itemsModel

                        objectName: "itemsModel"

                        ListElement { objectName: "pageSizeA0"; text: qsTr("A0"); pageSizeId: DSPageSize.A0 }
                        ListElement { objectName: "pageSizeA1"; text: qsTr("A1"); pageSizeId: DSPageSize.A1 }
                        ListElement { objectName: "pageSizeA2"; text: qsTr("A2"); pageSizeId: DSPageSize.A2 }
                        ListElement { objectName: "pageSizeA3"; text: qsTr("A3"); pageSizeId: DSPageSize.A3 }
                        ListElement { objectName: "pageSizeA4"; text: qsTr("A4"); pageSizeId: DSPageSize.A4 }
                        ListElement { objectName: "pageSizeA5"; text: qsTr("A5"); pageSizeId: DSPageSize.A5 }
                        ListElement { objectName: "pageSizeA6"; text: qsTr("A6"); pageSizeId: DSPageSize.A6 }
                        ListElement { objectName: "pageSizeCustom"; text: qsTr("Custom"); pageSizeId: DSPageSize.Custom }
                    }

                    MenuItem {
                        objectName: "menuItem"
                        text: model.text
                    }
                }
            }

            onCurrentIndexChanged: pageSizeColumn.pageSizeId = itemsModel.get(currentIndex).pageSizeId
        }

        Item {
            objectName: "pageSizeMmFields"
            width: parent.width
            height: pageWidthMmEdit.implicitHeight

            TextField {
                id: pageWidthMmEdit

                objectName: "pageWidthMmEdit"
                anchors {
                    left: parent.left
                    right: fieldsSeparator.left
                }
                label: qsTr("Page width")
                placeholderText: label
                validator: IntValidator { bottom: 1; top: 10000; }
                inputMethodHints: Qt.ImhDigitsOnly | Qt.ImhNoPredictiveText
                text: pageSizeColumn.pageSizeMm.width
                EnterKey.iconSource: "image://theme/icon-m-enter-next"

                EnterKey.onClicked: pageHeightMmEdit.focus = true
                onTextChanged: pageSizeColumn.pageSizeMm.width = text
            }

            Label {
                id: fieldsSeparator

                objectName: "fieldsSeparator"
                anchors {
                    top: parent.top
                    topMargin: pageWidthMmEdit.textTopMargin
                    leftMargin: pageWidthMmEdit.textTopMargin
                    rightMargin: pageWidthMmEdit.textTopMargin
                    horizontalCenter: parent.horizontalCenter
                }
                height: pageWidthMmEdit.contentItem.height
                text: qsTr("×");
            }

            TextField {
                id: pageHeightMmEdit

                objectName: "pageHeightMmEdit"
                anchors {
                    left: fieldsSeparator.right
                    right: parent.right
                }
                label: qsTr("Page height")
                placeholderText: label
                validator: IntValidator { bottom: 1; top: 10000; }
                inputMethodHints: Qt.ImhDigitsOnly | Qt.ImhNoPredictiveText
                text: pageSizeColumn.pageSizeMm.height
                EnterKey.iconSource: "image://theme/icon-m-enter-next"

                EnterKey.onClicked: pageWidthMmEdit.focus = true
                onTextChanged: pageSizeColumn.pageSizeMm.height = text
            }
        }
    }
}
