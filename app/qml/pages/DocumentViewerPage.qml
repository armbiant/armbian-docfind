/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import QtQml.Models 2.2
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0
import ru.auroraos.DocumentScanner 1.0
import "../controls"

Page {
    objectName: "documentViewerPage"
    allowedOrientations: Orientation.Portrait

    PageHeader {
        id: pageHeader

        objectName: "pageHeader"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        extraContent.children: [
            IconButton {
                objectName: "pageHeaderButton"
                anchors.verticalCenter: parent.verticalCenter
                icon {
                    source: "image://theme/icon-m-about"
                    sourceSize {
                        width: Theme.iconSizeMedium
                        height: Theme.iconSizeMedium
                    }
                }

                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
        title: qsTr("Document Scanner")
    }

    Loader {
        objectName: "loader"
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: createDocumentBtn.top
            bottomMargin: Theme.horizontalPageMargin
        }
        sourceComponent: Storage.documentsCount === 0 ? documentPlaceholderComponent : documentViewerComponent
    }

    Component {
        id: documentPlaceholderComponent

        Item {
            objectName: "documentPlaceholder"
            anchors.fill: parent

            Label {
                id: descrLabel

                objectName: "descrLabel"
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                padding: Theme.horizontalPageMargin
                text: qsTr("Document scanner allows you to save your documents using a smart camera")
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: Theme.fontSizeLarge
                color: Theme.secondaryColor
            }

            Image {
                objectName: "backgroundImg"
                anchors {
                    top: descrLabel.bottom
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                    margins: Theme.paddingLarge * 2
                }
                layer {
                    enabled: true
                    effect: ColorOverlay {
                        objectName: "colorOverlay"
                        color: palette.colorScheme === Theme.DarkOnLight ? Theme.darkPrimaryColor : Theme.lightPrimaryColor
                    }
                }
                fillMode: Image.PreserveAspectFit
                opacity: Theme.opacityFaint
                source: Qt.resolvedUrl("../images/GraphicFingersensor.svg")
            }
        }
    }

    Component {
        id: documentViewerComponent

        Item {
            objectName: "documentViewerContainer"
            anchors.fill: parent
            clip: true

            SilicaGridView {
                id: documentViewer

                objectName: "documentViewer"
                anchors {
                    fill: parent
                    leftMargin: Theme.paddingMedium + Theme.paddingSmall
                    rightMargin: Theme.paddingMedium + Theme.paddingSmall
                }
                cellWidth: width / 2
                cellHeight: cellWidth * (297 / 210)
                model: DSDocumentModel {
                    objectName: "documentModel"
                    storage: Storage
                }
                delegate: documentDelegate
                displaced: Transition {
                    objectName: "displacedTransition"

                    NumberAnimation {
                        objectName: "positionAnimation"
                        properties: "x,y"
                        duration: 200
                        easing.type: Easing.OutCubic
                    }
                }

                VerticalScrollDecorator {
                    objectName: "scrollDecorator"
                    anchors.rightMargin: -documentViewer.anchors.rightMargin
                }

                ViewPlaceholder {
                    objectName: "viewPlaceholder"
                    enabled: Storage.documentsCount === 0
                    text: qsTr("No documents yet")
                }

                Component {
                    id: documentDelegate

                    GridItem {
                        id: documentDelegateItem

                        function editDocument(index) {
                            var document = Storage.document(index);
                            pageStack.push(Qt.resolvedUrl("PageViewerPage.qml"), { document: document });
                        }

                        function removeDocument(document) {
                            remorseAction(qsTr("Deleting"), function() {
                                var documentIndex = Storage.indexOfDocument(document);
                                Storage.takeDocument(documentIndex);
                            });
                        }

                        objectName: "documentDelegateItem"
                        menu: contextMenu

                        ListView.onRemove: animateRemoval(documentDelegateItem)
                        onClicked: documentDelegateItem.editDocument(model.index)

                        Rectangle {
                            id: documentDelegateContent

                            objectName: "documentDelegateContent"
                            anchors {
                                fill: parent
                                margins: Theme.paddingSmall
                            }
                            color: Theme.lightPrimaryColor
                            border {
                                color: Theme.highlightDimmerFromColor(palette.colorScheme === Theme.DarkOnLight
                                                                      ? Theme.darkSecondaryColor
                                                                      : Theme.lightSecondaryColor, palette.colorScheme)
                                width: Theme.dp(1.0)
                            }

                            Image {
                                id: documentDelegateItemPreview

                                objectName: "documentDelegateItemPreview"
                                anchors {
                                    fill: parent
                                    margins: documentDelegateContent.border.width + Theme.paddingSmall
                                }
                                source: model.preview
                                asynchronous: true
                                cache: false
                                fillMode: Image.PreserveAspectFit

                                BusyIndicator {
                                    objectName: "busyIndicator"
                                    anchors.centerIn: parent
                                    size: BusyIndicatorSize.Medium
                                    running: documentDelegateItemPreview.status !== Image.Ready
                                }
                            }

                            Rectangle {
                                objectName: "documentDelegateItemLabelBckgr"
                                anchors {
                                    left: parent.left
                                    right: parent.right
                                    bottom: parent.bottom
                                    margins: documentDelegateContent.border.width + Theme.paddingSmall
                                }
                                height: documentDelegateItemLabel.contentHeight + Theme.paddingMedium
                                color: Theme.rgba(Theme.darkPrimaryColor, Theme.opacityLow)

                                Label {
                                    id: documentDelegateItemLabel

                                    objectName: "documentDelegateItemLabel"
                                    anchors {
                                        fill: parent
                                        margins: Theme.paddingMedium
                                    }
                                    font {
                                        pixelSize: Theme.fontSizeExtraSmallBase
                                        bold: false
                                    }
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                    wrapMode: Text.WordWrap
                                    color: Theme.highlightColor
                                    text: model.name
                                }
                            }
                        }

                        Component {
                            id: contextMenu

                            ContextMenu {
                                objectName: "contextMenu"

                                MenuItem {
                                    objectName: "removeDocumentMenuItem"
                                    text: qsTr("Remove document")

                                    onClicked: {
                                        var document = Storage.document(model.index);
                                        documentDelegateItem.removeDocument(document);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Button {
        id: createDocumentBtn

        objectName: "createDocumentBtn"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: Theme.horizontalPageMargin
        }
        border {
            color: Theme.rgba(color, Theme.opacityFaint)
            highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
        }
        text: qsTr("Create document")

        onClicked: pageStack.push(Qt.resolvedUrl("PhotoShootingPage.qml"));
    }
}
