/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.DocumentScanner 1.0
import "../controls"

Dialog {
    id: documentSaverPage

    property var document

    objectName: "documentSaverPage"
    allowedOrientations: Orientation.Portrait
    forwardNavigation: false

    DialogHeader {
        id: pageHeader

        objectName: "pageHeader"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        cancelText: qsTr("Back")
        acceptText: ""
        spacing: 0.0
    }

    SectionHeader {
        id: pageTitle

        objectName: "pageTitle"
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
        }
        leftPadding: Theme.horizontalPageMargin
        rightPadding: Theme.horizontalPageMargin
        text: qsTr("Save document")
    }

    SilicaFlickable {
        objectName: "flickable"
        anchors {
            top: pageTitle.bottom
            left: parent.left
            right: parent.right
            bottom: pageFooter.top
        }
        contentHeight: settingsColumn.height

        Column {
            id: settingsColumn

            objectName: "settingsColumn"
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingSmall

            TextField {
                id: documentNameEdit

                objectName: "documentNameEdit"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                label: qsTr("Document name")
                placeholderText: label
                text: document.name
                rightItem: IconButton {
                    objectName: "iconButton"
                    width: icon.width
                    height: icon.height
                    icon.source: "image://theme/icon-m-input-clear"
                    opacity: documentNameEdit.text.length > 0 ? 1.0 : 0.0

                    Behavior on opacity {
                        objectName: "behaviorOnOpacity"

                        FadeAnimation {
                            objectName: "fadeAnimation"
                        }
                    }

                    onClicked: documentNameEdit.text = ""
                }
                EnterKey.iconSource: "image://theme/icon-m-enter-close"

                EnterKey.onClicked: documentNameEdit.focus = false
            }

            PageSizeEditor {
                id: pagesSizeEdit

                objectName: "pagesSizeEdit"
                anchors {
                    left: parent.left
                    right: parent.right
                }
            }

            TextField {
                id: pagesDpiField

                property int pageDpi: 300

                objectName: "pagesDpiField"
                label: qsTr("Page DPI")
                placeholderText: label
                validator: IntValidator {
                    objectName: "intValidator"
                    bottom: 50
                    top: 300
                }
                inputMethodHints: Qt.ImhDigitsOnly | Qt.ImhNoPredictiveText
                text: pagesDpiField.pageDpi
                EnterKey.iconSource: "image://theme/icon-m-enter-close"

                EnterKey.onClicked: pagesDpiField.focus = false
                onTextChanged: pagesDpiField.pageDpi = parseInt(pagesDpiField.text)
            }

            TextSwitch {
                id: deleteFromDraftSwitch

                objectName: "deleteFromDraftSwitch"
                text: qsTr("Delete from draft")
                description: qsTr("Delete from draft after saving")
                checked: false
                visible: Storage.indexOfDocument(document) !== -1
            }
        }
    }

    Column {
        id: pageFooter

        objectName: "pageFooter"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: Theme.horizontalPageMargin
        }
        spacing: Theme.paddingSmall

        Button {
            objectName: "saveToDraftBtn"
            anchors {
                left: parent.left
                right: parent.right
            }
            border {
                color: Theme.rgba(color, Theme.opacityFaint)
                highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }
            backgroundColor: "transparent"
            text: qsTr("Save to draft")
            visible: Storage.indexOfDocument(document) === -1

            onClicked: {
                Storage.insertDocument(Storage.documentsCount, document);
                pageStack.replaceAbove(null, Qt.resolvedUrl("DocumentViewerPage.qml"));
            }
        }

        Button {
            objectName: "saveToDocumentBtn"
            anchors {
                left: parent.left
                right: parent.right
            }
            border {
                color: Theme.rgba(color, Theme.opacityFaint)
                highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }
            text: qsTr("Save to document")

            onClicked: {
                PdfWriter.save(document, documentNameEdit.text, pagesSizeEdit.pageSize,
                               pagesDpiField.pageDpi, deleteFromDraftSwitch.checked);
                pageStack.replaceAbove(null, Qt.resolvedUrl("DocumentViewerPage.qml"));
            }
        }
    }
}
