/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import QtQml.Models 2.2
import Sailfish.Silica 1.0
import ru.auroraos.DocumentScanner 1.0
import "../controls"

Dialog {
    id: pageViewerPage

    property var document
    property bool enableDragging: false

    objectName: "pageViewerPage"
    allowedOrientations: Orientation.Portrait
    forwardNavigation: !enableDragging
    backNavigation: !pageViewer._dragging && !enableDragging

    onAccepted: pageStack.push(Qt.resolvedUrl("DocumentSaverPage.qml"), { document: pageViewerPage.document });

    Connections {
        objectName: "connections"
        target: Storage

        onDocumentTaken: {
            if (document === pageViewerPage.document) {
                pageStack.replaceAbove(null, Qt.resolvedUrl("DocumentViewerPage.qml"));
            }
        }
    }

    DialogHeader {
        id: pageHeader

        objectName: "pageHeader"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        cancelText: qsTr("Back")
        acceptText: qsTr("Save")
        spacing: 0.0
    }

    SectionHeader {
        id: pageTitle

        objectName: "pageTitle"
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
        }
        leftPadding: Theme.horizontalPageMargin
        rightPadding: Theme.horizontalPageMargin
        text: pageViewerPage.enableDragging ? qsTr("Change the order of the pages")
                                            : qsTr("Scanned %1 p.").arg(document.pagesCount)
    }

    Item {
        objectName: "documentViewerContainer"
        anchors {
            top: pageTitle.bottom
            left: parent.left
            right: parent.right
            bottom: pageFooter.top
            bottomMargin: Theme.horizontalPageMargin
        }
        clip: true

        SilicaGridView {
            id: pageViewer

            property bool _dragging: false
            property bool _pressedAndHeld: false
            property int _scrollingDirection: 0

            objectName: "pageViewer"
            anchors {
                fill: parent
                leftMargin: Theme.paddingMedium + Theme.paddingSmall
                rightMargin: Theme.paddingMedium + Theme.paddingSmall
            }
            cellWidth: width / 2
            cellHeight: cellWidth * (297 / 210)
            model: DelegateModel {
                model: DSPageModel {
                    objectName: "pageModel"
                    document: pageViewerPage.document
                }
                delegate: pageDelegate
            }
            displaced: Transition {
                objectName: "displacedTransition"

                NumberAnimation {
                    objectName: "positionAnimation"
                    properties: "x,y"
                    duration: 200
                    easing.type: Easing.OutCubic
                }
            }

            on_DraggingChanged: {
                if (!_dragging) {
                    _scrollingDirection = 0;
                }
            }

            VerticalScrollDecorator {
                objectName: "scrollDecorator"
                anchors.rightMargin: -pageViewer.anchors.rightMargin
            }

            ViewPlaceholder {
                objectName: "viewPlaceholder"
                enabled: pageViewerPage.document.pagesCount === 0
                text: qsTr("No pages yet")
            }

            Component {
                id: pageDelegate

                GridItem {
                    id: pageDelegateItem

                    property int visualIndex: DelegateModel.itemsIndex
                    property bool pressedAndHeld: false
                    property bool pressed: containsPress

                    function editPage(index) {
                        pageStack.push(Qt.resolvedUrl("PageEditorPage.qml"), {
                                           document: pageViewerPage.document,
                                           pageIndex: index
                                       });
                    }

                    function removePage(page) {
                        remorseAction(qsTr("Deleting"), function() {
                            var pageIndex = pageViewerPage.document.indexOfPage(page);
                            pageViewerPage.document.takePage(pageIndex);
                        });
                    }

                    objectName: "pageDelegateItem"
                    menu: contextMenu
                    openMenuOnPressAndHold: pageViewerPage.enableDragging ? false : true
                    opacity: pageViewer._pressedAndHeld && !pressedAndHeld ? Theme.opacityLow : 1.0
                    drag.target: pageViewerPage.enableDragging && pressedAndHeld ? pageDelegateContent : undefined

                    ListView.onRemove: animateRemoval(pageDelegateItem)
                    onClicked: {
                        if (!pageViewerPage.enableDragging) {
                            pageDelegateItem.editPage(model.index);
                        }
                    }
                    onPressAndHold: {
                        if (pageViewerPage.enableDragging) {
                            pageViewer._pressedAndHeld = true;
                            pressedAndHeld = true;
                        }
                    }
                    onReleased: {
                        pageViewer._pressedAndHeld = false;
                        pressedAndHeld = false;
                    }

                    Behavior on opacity {
                        objectName: "opacityBehavior"

                        NumberAnimation {
                            objectName: "opacityAnimation"
                            duration: 100
                        }
                    }

                    SmoothedAnimation {
                        objectName: "contentYUpAnimation"
                        target: pageViewer
                        property: "contentY"
                        to: 0
                        running: pageViewer._scrollingDirection === -1
                    }

                    SmoothedAnimation {
                        objectName: "contentYDownAnimation"
                        target: pageViewer
                        property: "contentY"
                        to: pageViewer.contentHeight - pageViewer.height
                        running: pageViewer._scrollingDirection === 1
                    }

                    Rectangle {
                        id: pageDelegateContent

                        objectName: "pageDelegateContent"
                        anchors {
                            horizontalCenter: parent.horizontalCenter
                            verticalCenter: parent.verticalCenter
                        }
                        width: pageViewer.cellWidth - Theme.paddingMedium
                        height: pageViewer.cellHeight - Theme.paddingMedium
                        color: Theme.lightPrimaryColor
                        scale: pageViewerPage.enableDragging && !pageDelegateItem.pressed ? 0.95 : 1.0
                        border {
                            color: Theme.highlightDimmerFromColor(palette.colorScheme === Theme.DarkOnLight
                                                                  ? Theme.darkSecondaryColor
                                                                  : Theme.lightSecondaryColor, palette.colorScheme)
                            width: Theme.dp(1.0)
                        }
                        states: [
                            State {
                                objectName: "activeState"
                                when: pageDelegateContent.Drag.active

                                ParentChange {
                                    objectName: "parentChange"
                                    target: pageDelegateContent
                                    parent: pageViewer
                                }

                                AnchorChanges {
                                    objectName: "anchorChanges"
                                    target: pageDelegateContent
                                    anchors {
                                        horizontalCenter: undefined
                                        verticalCenter: undefined
                                    }
                                }

                                PropertyChanges {
                                    objectName: "propertyChanges"
                                    target: pageDelegateContent
                                    scale: 1.0
                                }
                            }
                        ]

                        Drag.active: pageDelegateItem.drag.active
                        Drag.source: pageDelegateItem
                        Drag.hotSpot.x: width / 2
                        Drag.hotSpot.y: height / 2
                        Drag.onActiveChanged: {
                            pageViewer._dragging = Drag.active;

                            if (!Drag.active) {
                                pageViewer._pressedAndHeld = false;
                                pageDelegateItem.pressedAndHeld = false;
                            }
                        }

                        Behavior on scale {
                            objectName: "scaleChanges"

                            NumberAnimation {
                                objectName: "scaleAnimation"
                                duration: 100
                            }
                        }

                        Image {
                            id: pageDelegateItemPreview

                            objectName: "pageDelegateItemPreview"
                            anchors {
                                fill: parent
                                margins: pageDelegateContent.border.width + Theme.paddingSmall
                            }
                            source: model.previewImage
                            asynchronous: true
                            cache: false
                            fillMode: Image.PreserveAspectFit

                            BusyIndicator {
                                objectName: "busyIndicator"
                                anchors.centerIn: parent
                                size: BusyIndicatorSize.Medium
                                running: pageDelegateItemPreview.status !== Image.Ready
                            }
                        }

                        Rectangle {
                            objectName: "pageDelegateItemLabelBckgr"
                            anchors {
                                horizontalCenter: parent.horizontalCenter
                                bottom: parent.bottom
                                margins: pageDelegateContent.border.width + Theme.paddingSmall
                            }
                            width: pageDelegateItemLabel.contentWidth + Theme.paddingLarge
                            height: pageDelegateItemLabel.contentHeight + Theme.paddingMedium
                            color: Theme.rgba(Theme.darkPrimaryColor, Theme.opacityLow)

                            Label {
                                id: pageDelegateItemLabel

                                objectName: "pageDelegateItemLabel"
                                anchors {
                                    fill: parent
                                    margins: Theme.paddingMedium
                                }
                                font {
                                    pixelSize: Theme.fontSizeExtraSmallBase
                                    bold: false
                                }
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                color: Theme.highlightColor
                                text: model.index + 1
                            }
                        }
                    }

                    DropArea {
                        objectName: "dropArea"
                        anchors {
                            fill: parent
                            margins: Theme.paddingLarge * 2
                        }

                        onEntered: {
                            var indexFrom = drag.source.visualIndex;
                            var indexTo = pageDelegateItem.visualIndex;
                            pageViewerPage.document.movePage(indexFrom, indexTo);
                        }
                        onPositionChanged: {
                            var object = mapToItem(pageViewer, drag.x, drag.y);
                            var centerY = object.y + drag.source.height / 2;
                            var top = pageViewer.y;
                            var bottom = top + pageViewer.height;
                            var scrollAreaHeight = pageViewer.height / 4;

                            if (centerY < top + scrollAreaHeight) {
                                pageViewer._scrollingDirection = -1;
                            } else if (centerY > bottom - scrollAreaHeight) {
                                pageViewer._scrollingDirection = 1;
                            } else {
                                pageViewer._scrollingDirection = 0;
                            }
                        }
                    }

                    Component {
                        id: contextMenu

                        ContextMenu {
                            objectName: "contextMenu"

                            MenuItem {
                                objectName: "removePageMenuItem"
                                text: qsTr("Remove page")

                                onClicked: {
                                    var page = pageViewerPage.document.page(model.index);
                                    pageDelegateItem.removePage(page);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Column {
        id: pageFooter

        objectName: "pageFooter"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: Theme.horizontalPageMargin
        }
        spacing: Theme.paddingSmall

        Button {
            objectName: "changeOrderBtn"
            anchors {
                left: parent.left
                right: parent.right
            }
            border {
                color: Theme.rgba(color, Theme.opacityFaint)
                highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }
            backgroundColor: "transparent"
            text: pageViewerPage.enableDragging ? qsTr("Done") : qsTr("Change page's order")
            enabled: pageViewerPage.document.pagesCount > 1

            onClicked: pageViewerPage.enableDragging = !pageViewerPage.enableDragging
        }

        Button {
            objectName: "addPageBtn"
            anchors {
                left: parent.left
                right: parent.right
            }
            border {
                color: Theme.rgba(color, Theme.opacityFaint)
                highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }
            text: qsTr("Add page")
            enabled: !pageViewerPage.enableDragging

            onClicked: pageStack.push(Qt.resolvedUrl("PhotoShootingPage.qml"), { document: pageViewerPage.document });
        }
    }
}
