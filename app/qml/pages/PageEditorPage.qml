/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.auroraos.DocumentScanner 1.0
import "../controls"

Dialog {
    id: pageEditorPage

    property var document
    property int pageIndex: -1

    objectName: "pageViewerPage"
    allowedOrientations: Orientation.Portrait
    forwardNavigation: false
    backNavigation: !loader._pointIsCaptured
    showNavigationIndicator: !loader._pointIsCaptured

    Component.onCompleted: {
        for (var i = 0; i < document.pagesCount; ++i) {
            var page = document.page(i);
            page.cache = true;
        }
    }
    Component.onDestruction: {
        for (var i = 0; i < document.pagesCount; ++i) {
            var page = document.page(i);
            page.cache = false;
        }
    }
    onPageIndexChanged: {
        if (pageIndex < 0 || pageIndex >= document.pagesCount) {
            return;
        }

        var page = document.page(pageIndex);
        if (page) {
            loader._page = page;
        }
    }

    Connections {
        objectName: "connections"
        target: Storage

        onDocumentTaken: {
            if (document === pageEditorPage.document) {
                pageStack.replaceAbove(null, Qt.resolvedUrl("DocumentViewerPage.qml"));
            }
        }
    }

    DialogHeader {
        id: pageHeader

        objectName: "pageHeader"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        cancelText: qsTr("Back")
        acceptText: ""
        spacing: 0.0
    }

    SectionHeader {
        id: pageTitle

        objectName: "pageTitle"
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
        }
        leftPadding: Theme.horizontalPageMargin
        rightPadding: Theme.horizontalPageMargin
        text: document.name
    }

    Loader {
        id: loader

        property var _page
        property bool _showPreview: true
        property bool _pointIsCaptured: false

        objectName: "loader"
        anchors {
            top: pageTitle.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        sourceComponent: _showPreview ? previewAreaComponent : editAreaComponent
    }

    Component {
        id: previewAreaComponent

        Item {
            id: previewArea

            objectName: "previewArea"
            anchors.fill: parent

            Item {
                objectName: "previewInnerArea"
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                    bottom: previewAreaFooter.top
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    bottomMargin: Theme.horizontalPageMargin
                }

                Rectangle {
                    id: paperArea

                    property size areaSize: DSHelper.scale(Qt.size(parent.width, parent.width * (297 / 210)),
                                                           Qt.size(parent.width, parent.height))

                    objectName: "paperArea"
                    anchors.centerIn: parent
                    width: areaSize.width
                    height: areaSize.height
                    color: Theme.lightPrimaryColor
                    border {
                        color: Theme.highlightDimmerFromColor(palette.colorScheme === Theme.DarkOnLight
                                                              ? Theme.darkSecondaryColor
                                                              : Theme.lightSecondaryColor, palette.colorScheme)
                        width: Theme.dp(1.0)
                    }

                    Image {
                        id: previewImage

                        objectName: "previewImage"
                        anchors {
                            fill: parent
                            margins: paperArea.border.width + Theme.paddingSmall
                        }
                        source: loader._page.previewImagePath
                        asynchronous: true
                        cache: false
                        fillMode: Image.PreserveAspectFit

                        BusyIndicator {
                            objectName: "busyIndicator"
                            anchors.centerIn: parent
                            size: BusyIndicatorSize.Medium
                            running: previewImage.status !== Image.Ready
                        }
                    }

                    Rectangle {
                        objectName: "pageNumberBckgr"
                        anchors {
                            horizontalCenter: parent.horizontalCenter
                            bottom: parent.bottom
                            margins: paperArea.border.width + Theme.paddingSmall
                        }
                        width: pageNumberLbl.contentWidth + Theme.paddingLarge
                        height: pageNumberLbl.contentHeight + Theme.paddingMedium
                        color: Theme.rgba(Theme.darkPrimaryColor, Theme.opacityLow)

                        Label {
                            id: pageNumberLbl

                            objectName: "pageNumberLbl"
                            anchors {
                                fill: parent
                                margins: Theme.paddingMedium
                            }
                            font {
                                pixelSize: Theme.fontSizeExtraSmallBase
                                bold: false
                            }
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            color: Theme.highlightColor
                            text: "%1 / %2".arg(pageIndex + 1).arg(document.pagesCount)
                        }
                    }
                }
            }

            Item {
                id: previewAreaFooter

                objectName: "previewAreaFooter"
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                height: changePageBtn.height + Theme.horizontalPageMargin

                Button {
                    id: prevPageBtn

                    objectName: "prevPageBtn"
                    anchors {
                        left: parent.left
                        bottom: parent.bottom
                        margins: Theme.horizontalPageMargin
                    }
                    width: height
                    border {
                        color: Theme.rgba(color, Theme.opacityFaint)
                        highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                    }
                    icon.source: "image://theme/icon-m-left"
                    enabled: pageIndex > 0

                    onClicked: --pageIndex
                }

                Button {
                    id: nextPageBtn

                    objectName: "nextPageBtn"
                    anchors {
                        right: parent.right
                        bottom: parent.bottom
                        margins: Theme.horizontalPageMargin
                    }
                    width: height
                    border {
                        color: Theme.rgba(color, Theme.opacityFaint)
                        highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                    }
                    icon.source: "image://theme/icon-m-right"
                    enabled: pageIndex < document.pagesCount - 1

                    onClicked: ++pageIndex
                }

                Button {
                    id: changePageBtn

                    objectName: "changePageBtn"
                    anchors {
                        left: prevPageBtn.right
                        right: nextPageBtn.left
                        bottom: parent.bottom
                        margins: Theme.horizontalPageMargin
                    }
                    border {
                        color: Theme.rgba(color, Theme.opacityFaint)
                        highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                    }
                    backgroundColor: "transparent"
                    text: qsTr("Change")

                    onClicked: loader._showPreview = false
                }
            }
        }
    }

    Component {
        id: editAreaComponent

        Item {
            id: editArea

            objectName: "editArea"
            anchors.fill: parent

            Item {
                objectName: "editInnerArea"
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                    bottom: editAreaFooter.top
                    bottomMargin: Theme.horizontalPageMargin
                }

                DSPhotoEditor {
                    id: photoEditor

                    objectName: "photoEditor"
                    anchors.fill: parent
                    frame: loader._page.frame
                    overlayColor: Theme.rgba(Theme.darkPrimaryColor, Theme.opacityLow)
                    pointsColor: Theme.lightPrimaryColor
                    pointsHighlightedColor: Theme.highlightColor
                    pointsVisible: true
                    pointsManualMoving: true

                    onPointIsCapturedChanged: loader._pointIsCaptured = pointIsCaptured
                }

                DSPhotoAligner {
                    id: photoAligner

                    objectName: "photoAligner"
                    active: true
                    frame: photoEditor.frame
                }
            }

            Item {
                id: editAreaFooter

                objectName: "editAreaFooter"
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                height: doneBtn.height + Theme.horizontalPageMargin

                Button {
                    id: rotateLeftBtn

                    objectName: "rotateLeftBtn"
                    anchors {
                        left: parent.left
                        bottom: parent.bottom
                        margins: Theme.horizontalPageMargin
                    }
                    width: height
                    border {
                        color: Theme.rgba(color, Theme.opacityFaint)
                        highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                    }
                    icon.source: "image://theme/icon-m-rotate-left"

                    onClicked: photoEditor.rotateLeft()
                }

                Button {
                    id: rotateRightBtn

                    objectName: "rotateRightBtn"
                    anchors {
                        right: parent.right
                        bottom: parent.bottom
                        margins: Theme.horizontalPageMargin
                    }
                    width: height
                    border {
                        color: Theme.rgba(color, Theme.opacityFaint)
                        highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                    }
                    icon.source: "image://theme/icon-m-rotate-right"

                    onClicked: photoEditor.rotateRight()
                }

                Button {
                    id: doneBtn

                    objectName: "doneBtn"
                    anchors {
                        left: rotateLeftBtn.right
                        right: rotateRightBtn.left
                        bottom: parent.bottom
                        margins: Theme.horizontalPageMargin
                    }
                    border {
                        color: Theme.rgba(color, Theme.opacityFaint)
                        highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                    }
                    backgroundColor: "transparent"
                    text: qsTr("Done")
                    enabled: !photoAligner.processing

                    onClicked: {
                        loader._page.frame = photoAligner.frame;
                        loader._showPreview = true;
                    }
                }
            }
        }
    }
}
