/*******************************************************************************
**
** Copyright (C) 2021-2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Document Scanner project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.6
import QtMultimedia 5.6
import Sailfish.Silica 1.0
import ru.auroraos.DocumentScanner 1.0

Dialog {
    id: photoShootingPage

    property var document

    objectName: "photoShootingPage"
    allowedOrientations: Orientation.Portrait
    forwardNavigation: !photoEditor.pointIsCaptured && photoRecognizer.isPhoto && !photoRecognizer.processing
    backNavigation: !photoEditor.pointIsCaptured
    showNavigationIndicator: !photoEditor.pointIsCaptured

    onAccepted: {
        var page = DSHelper.createPage();
        page.frame = photoAligner.frame;

        var needCreate = !photoShootingPage.document;
        if (needCreate) {
            photoShootingPage.document = DSHelper.createDocument();
        }

        photoShootingPage.document.insertPage(document.pagesCount, page);
        page.cache = false;

        if (needCreate)
            pageStack.replace(Qt.resolvedUrl("PageViewerPage.qml"), { document: photoShootingPage.document });
        else
            pageStack.pop();
    }

    Connections {
        objectName: "connections"
        target: Storage

        onDocumentTaken: {
            if (document === photoShootingPage.document) {
                pageStack.replaceAbove(null, Qt.resolvedUrl("DocumentViewerPage.qml"));
            }
        }
    }

    DialogHeader {
        id: pageHeader

        objectName: "pageHeader"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        cancelText: qsTr("Back")
        acceptText: qsTr("Accept")
        spacing: 0
        visible: opacity > 0.0
        opacity: !photoRecognizer.photoInProcess ? 1.0 : 0.0

        Behavior on opacity {
            objectName: "behaviorOnOpacity"

            FadeAnimation {
                objectName: "fadeAnimation"
                duration: 100
            }
        }
    }

    DSPhotoCamera {
        id: photoCamera

        objectName: "photoCamera"
    }

    DSPhotoRecognizer {
        id: photoRecognizer

        property bool isPhoto: frameType === DSFrame.Photo
        property bool photoInProcess: isPhoto && processing

        objectName: "photoRecognizer"
        frame: photoCamera.frame
        active: true
    }

    DSPhotoAligner {
        id: photoAligner

        property bool isPhoto: frameType === DSFrame.Photo
        property bool photoInProcess: isPhoto && processing

        objectName: "photoAligner"
        frame: photoEditor.frame
        active: isPhoto
    }

    DSPhotoEditor {
        id: photoEditor

        objectName: "photoEditor"
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: pageFooter.top
            bottomMargin: Theme.horizontalPageMargin
        }
        frame: photoRecognizer.frame
        overlayColor: Theme.rgba(Theme.darkPrimaryColor, Theme.opacityLow)
        pointsColor: Theme.lightPrimaryColor
        pointsHighlightedColor: Theme.highlightColor
        pointsVisible: !photoRecognizer.photoInProcess
        pointsManualMoving: photoRecognizer.isPhoto

        Rectangle {
            x: photoEditor.imageFilledRect.x
            y: photoEditor.imageFilledRect.y
            width: photoEditor.imageFilledRect.width
            height: photoEditor.imageFilledRect.height
            color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityOverlay)
            visible: opacity > 0.0
            opacity: photoRecognizer.photoInProcess ? 1.0 : 0.0

            Behavior on opacity {
                objectName: "behaviorOnOpacity"

                FadeAnimation {
                    objectName: "fadeAnimation"
                    duration: 100
                }
            }

            Item {
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
                height: column.height

                Column {
                    id: column

                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    spacing: Theme.paddingMedium

                    BusyIndicator {
                        size: BusyIndicatorSize.Large
                        anchors.horizontalCenter: parent.horizontalCenter
                        running: photoRecognizer.photoInProcess
                    }

                    Label {
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        horizontalAlignment: Text.AlignHCenter
                        color: Theme.highlightColor
                        text: qsTr("Document processing")
                    }
                }
            }
        }
    }

    Item {
        id: pageFooter

        objectName: "pageFooter"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: shootBtn.height + Theme.horizontalPageMargin
        visible: opacity > 0.0
        opacity: !photoRecognizer.photoInProcess ? 1.0 : 0.0

        Behavior on opacity {
            objectName: "behaviorOnOpacity"

            FadeAnimation {
                objectName: "fadeAnimation"
                duration: 100
            }
        }

        Button {
            id: rotateLeftBtn

            objectName: "rotateLeftBtn"
            anchors {
                left: parent.left
                bottom: parent.bottom
                margins: Theme.horizontalPageMargin
            }
            width: height
            border {
                color: Theme.rgba(color, Theme.opacityFaint)
                highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }
            icon.source: "image://theme/icon-m-rotate-left"
            visible: photoRecognizer.isPhoto && !photoRecognizer.processing

            onClicked: photoEditor.rotateLeft()
        }

        Button {
            id: rotateRightBtn

            objectName: "rotateRightBtn"
            anchors {
                right: parent.right
                bottom: parent.bottom
                margins: Theme.horizontalPageMargin
            }
            width: height
            border {
                color: Theme.rgba(color, Theme.opacityFaint)
                highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }
            icon.source: "image://theme/icon-m-rotate-right"
            visible: photoRecognizer.isPhoto && !photoRecognizer.processing

            onClicked: photoEditor.rotateRight()
        }

        Button {
            id: shootBtn

            objectName: "shootBtn"
            anchors {
                left: photoRecognizer.isPhoto ? rotateLeftBtn.right : parent.left
                right: photoRecognizer.isPhoto ? rotateRightBtn.left : parent.right
                bottom: parent.bottom
                margins: Theme.horizontalPageMargin
            }
            border {
                color: Theme.rgba(color, Theme.opacityFaint)
                highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }
            backgroundColor: photoRecognizer.isPhoto ? "transparent" : Theme.rgba(color, Theme.opacityFaint)
            text: photoRecognizer.isPhoto ? qsTr("Reshoot") : qsTr("Shoot")

            onClicked: photoRecognizer.isPhoto ? photoCamera.clearPhoto() : photoCamera.catchPhoto()
        }
    }
}
