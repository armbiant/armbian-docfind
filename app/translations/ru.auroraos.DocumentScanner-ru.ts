<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;В проекте приведен пример использования OpenCV для поиска документов.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="73"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="83"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2021–2022 Open Mobile Platform LLC&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
    &lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
    &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
    &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>DSPdfWriter</name>
    <message>
        <location filename="../src/pdfwriter.cpp" line="155"/>
        <source>Document Scanner</source>
        <translation>Сканер документов</translation>
    </message>
    <message>
        <location filename="../src/pdfwriter.cpp" line="147"/>
        <source>Open document</source>
        <translation>Открыть документ</translation>
    </message>
    <message>
        <location filename="../src/pdfwriter.cpp" line="156"/>
        <source>Document has been saved</source>
        <translation>Документ был сохранен</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="47"/>
        <source>Document Scanner</source>
        <translation>Сканер документов</translation>
    </message>
</context>
<context>
    <name>DocumentSaverPage</name>
    <message>
        <location filename="../qml/pages/DocumentSaverPage.qml" line="62"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentSaverPage.qml" line="78"/>
        <source>Save document</source>
        <translation>Сохранить документ</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentSaverPage.qml" line="109"/>
        <source>Document name</source>
        <translation>Название документа</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentSaverPage.qml" line="170"/>
        <source>Delete from draft after saving</source>
        <translation>Удалить из черновиков после сохранения</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentSaverPage.qml" line="200"/>
        <source>Save to draft</source>
        <translation>Сохранить в черновики</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentSaverPage.qml" line="219"/>
        <source>Save to document</source>
        <translation>Сохранить в документы</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentSaverPage.qml" line="150"/>
        <source>Page DPI</source>
        <translation>DPI страниц</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentSaverPage.qml" line="169"/>
        <source>Delete from draft</source>
        <translation>Удалить из черновиков</translation>
    </message>
</context>
<context>
    <name>DocumentViewerPage</name>
    <message>
        <location filename="../qml/pages/DocumentViewerPage.qml" line="74"/>
        <source>Document Scanner</source>
        <translation>Сканер документов</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentViewerPage.qml" line="106"/>
        <source>Document scanner allows you to save your documents using a smart camera</source>
        <translation>Сканер документов позволяет сохранять ваши документы с помощью смарт-камеры</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentViewerPage.qml" line="312"/>
        <source>Create document</source>
        <translation>Создать документ</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentViewerPage.qml" line="179"/>
        <source>No documents yet</source>
        <translation>Документов еще нет</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentViewerPage.qml" line="194"/>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
    <message>
        <location filename="../qml/pages/DocumentViewerPage.qml" line="283"/>
        <source>Remove document</source>
        <translation>Удалить документ</translation>
    </message>
</context>
<context>
    <name>PageEditorPage</name>
    <message>
        <location filename="../qml/pages/PageEditorPage.qml" line="99"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageEditorPage.qml" line="408"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageEditorPage.qml" line="293"/>
        <source>Change</source>
        <translation>Изменить</translation>
    </message>
</context>
<context>
    <name>PageSizeEditor</name>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="96"/>
        <source>Page size:</source>
        <translation>Размер страницы:</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="97"/>
        <source>Select the desired page size ID</source>
        <translation>Выберите желаемый размер страницы</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="106"/>
        <source>A0</source>
        <translation>A0</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="107"/>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="108"/>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="109"/>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="110"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="111"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="112"/>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="113"/>
        <source>Custom</source>
        <translation>Пользовательский</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="139"/>
        <source>Page width</source>
        <translation>Ширина страницы</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="162"/>
        <source>×</source>
        <translation>×</translation>
    </message>
    <message>
        <location filename="../qml/controls/PageSizeEditor.qml" line="173"/>
        <source>Page height</source>
        <translation>Высота страницы</translation>
    </message>
</context>
<context>
    <name>PageViewerPage</name>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="156"/>
        <source>No pages yet</source>
        <translation>Страниц еще нет</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="388"/>
        <source>Remove page</source>
        <translation>Удалить страницу</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="425"/>
        <source>Change page&apos;s order</source>
        <translation>Изменить порядок страниц</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="441"/>
        <source>Add page</source>
        <translation>Добавить страницу</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="78"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="79"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="425"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="177"/>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="94"/>
        <source>Change the order of the pages</source>
        <translation>Изменить порядок страниц</translation>
    </message>
    <message>
        <location filename="../qml/pages/PageViewerPage.qml" line="95"/>
        <source>Scanned %1 p.</source>
        <translation>Отсканировано %1 стр.</translation>
    </message>
</context>
<context>
    <name>PhotoShootingPage</name>
    <message>
        <location filename="../qml/pages/PhotoShootingPage.qml" line="93"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../qml/pages/PhotoShootingPage.qml" line="94"/>
        <source>Accept</source>
        <translation>Принять</translation>
    </message>
    <message>
        <location filename="../qml/pages/PhotoShootingPage.qml" line="287"/>
        <source>Reshoot</source>
        <translation>Переснять</translation>
    </message>
    <message>
        <location filename="../qml/pages/PhotoShootingPage.qml" line="287"/>
        <source>Shoot</source>
        <translation>Снять</translation>
    </message>
    <message>
        <location filename="../qml/pages/PhotoShootingPage.qml" line="203"/>
        <source>Document processing</source>
        <translation>Документ обрабатывается</translation>
    </message>
</context>
</TS>
